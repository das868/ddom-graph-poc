# `ddom-graph-poc`
## Brief project overview / some remarks
...a to-be-expanded proof-of-concept repo for storing code. The goal is to test the translation process of dDOM into a graph and how useful this idea might be.

Both the C and Haskell implementations are to be considered ditched attempts since they are overly complicated and artificially hard to use, all while functional style Python3 offers a much more sleek syntax for the same output structures. And since this is only a proof-of-concept and it won't have to deal with the entirety of the project data just yet, performance is not the most critical issue here.

## Necessary dependencies
- [x] `py` in the form of a working Python3 interpreter
- [ ] `gcc` or rather `cc`, the GNU C Compiler
- [ ] `ghc` or rather `ghci`, the Glasgow Haskell Compiler

In the future, more items may be added to the list or some may be removed since development is still at a very early stage.

## Interesting reading material
- Article about the pathfinder algorithm in Python: [finxter.com](https://blog.finxter.com/daily-python-puzzle-graph-algorithm-pathfinder/)
- Article about dataclasses in Python: [typeclasses.com](https://typeclasses.com/python/data-classes)
- Manual to the DOT Graph Visualization language: [graphviz.com](https://www.graphviz.org/pdf/dotguide.pdf)

## Caveat
Expect massive and sudden changes at any point until the first release (if ever so).