# -*- coding: utf-8 -*-
from dataclasses import dataclass, replace
from enum import Enum, unique, auto
import typing
import json

@unique
class Node_Color(Enum):
    YELLOW = auto()
    GREEN = auto()
    RED = auto()

@unique
class Edge_Color(Enum):
    BLUE = auto()
    LILA = auto()
    TURKIS = auto()

UID = int

def inc_uid(uid: UID) -> UID:
    return uid + 2

@dataclass
class Node:
    node_uid: UID
    text: str
    node_color: Node_Color

@dataclass
class Edge:
    edge_uid: UID
    root: Node
    target: Node
    edge_color: Edge_Color

def parse_from_json_raw(path="sample.json") -> list : 
    # returns list of json objs = py dicts containing lemma infos
    f = open(path, "r")
    content = f.read()
    f.close()
    raw = json.loads(content)
    # check for parsing style
    root_on_top = "ROOT" in raw
    if root_on_top:
        parsed = raw["ROOT"]
    else:
        parsed = raw
    # DEBUG
    print(" [ + ]\tparsing from JSON successful")
    return parsed

def get_color(color_txt: str):
    test = color_txt.lower()
    if test == "yellow":
        return Node_Color.YELLOW
    elif test == "green":
        return Node_Color.GREEN
    elif test == "red":
        return Node_Color.RED
    elif test == "blue":
        return Edge_Color.BLUE
    elif test == "lila":
        return Edge_Color.LILA
    elif test == "turkis":
        return Edge_Color.TURKIS
    else:
        return None

def parse_from_json_parsed(json_input: list):
    res_list = list()
    for lemma in json_input:
        res = None
        if lemma["color"] in "yellow green red":
            res = Node(1, lemma["text"], get_color(lemma["color"]))
        elif lemma["color"] in "blue lila turkis":
            root = Node(3, lemma["text"], Node_Color.YELLOW) # TODO: add proper root support in JSON
            t_help = lemma["target"]
            target = Node(4, t_help["text"], t_help["color"])
            res = Edge(2, root, target, get_color(lemma["color"]))
        else:
            res = Node((-1), "§§§!ERROR!", Node_Color.RED)
        res_list.append(res)
    # DEBUG
    print(" [ + ]\tparsing from refined JSON to lemma list successful")
    return res_list

# TEMP only for debugging purposes
def test_behavior():
    print(parse_from_json_parsed(parse_from_json_raw()))

if __name__ == "__main__":
    test_behavior()

# TODOs:
# + proper printing: already present in print
# - unify json model for storing lemmata: incl. or excl. "ROOT" obj?
# - unify data model for storing lemmata: diff datastructures for node/edge or nah?
# - add datastructure for (Sub)Graph
# - add file parser
# - add subgraph parser
# - add subgraph sorting algorithm
# - output finished lemma-list to file for other uses
# - [OPT] add DOT visualization output support

# final sort must operate on target of edges!!!