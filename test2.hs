module Main where

data Node_Color = 
    Yellow
    | Green
    | Red
    deriving(Eq,Show)

data Edge_Color =
    Blue
    | Lila
    | Turkis
    deriving(Eq,Show)

type Uid = Integer

inc_uid :: Uid -> Uid
inc_uid uid = uid + 2

data Node = Node 
    { node_uid :: Uid
    , text :: String
    , node_color :: Node_Color
    }
    deriving(Eq,Show)

data Edge = Edge
    { edge_uid :: Uid
    , root :: Node
    , target :: Node
    , edge_color :: Edge_Color
    }
    deriving(Eq,Show)

data Graph = Graph
    { graph_uid :: Uid
    , nodes :: [Node]
    , edges :: [Edge]
    }
    deriving(Eq,Show)

create_node :: Uid -> String -> Node_Color -> (Uid, Node)
create_node u str nc = 
    (u', x)
    where 
        u' = inc_uid u
        x = Node 
            { node_uid=u
            , text = str
            , node_color = nc
            }

create_edge :: Uid -> Node -> Node -> Edge_Color -> (Uid, Edge)
create_edge u rt tg ec =
    (u', x)
    where
        u' = inc_uid u
        x = Edge
            { edge_uid = u
            , root = rt
            , target = tg
            , edge_color = ec
            }

create_edge_from_parts :: Uid -> Uid -> String -> Node_Color -> String -> Node_Color -> Edge_Color -> (Uid, Uid, Edge)
-- always nodes_uid before edges_uid
create_edge_from_parts un ue rt rc tt tc ec =
    (un'', ue', x)
    where
        (un', roo) = create_node un rt rc
        (un'', tar) = create_node un' tt tc
        (ue', x) = create_edge ue roo tar ec

-- TODO: add proper printing for graphs

-- top debugging code
first :: (a,b,c) -> a
first (a, _, _) = a

second :: (a,b,c) -> b
second (_, b, _) = b

third :: (a,b,c) -> c
third (_, _, c) = c

t = create_node 123 "hahUUU" Red       
s = create_node 125 "öhöömm" Green     
e = create_edge 42 (snd t) (snd s) Blue
f = create_edge_from_parts 123 42 "hahUUU" Red "öhöömm" Green Blue
test = e == (second f, third f)
-- end debugging code

main :: IO()
main = putStrLn "Hello World!"