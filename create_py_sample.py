# -*- coding: utf-8 -*-
import json

# SAMPLE LEMMAS
#   cerfolh 503608, yellow, "selbst erstellt, OK?"
#   cerna 503677, red, ""
#   cernelha 503687, green, ""
#   cenofali 502652, gelb, ""  ->  cenophali [ID 123456] (wenn blau nicht da, erstellen)

def create_sample_json(output : str = "file"):
    # output can be "file" or "text"; default is "file"
    sample_json = dict()
    lemma_obj_list = list()
    lemma_obj_list.append({ "text" : "cerfolh", "ID" : 503608, "color" : "yellow", "target" : {}, "comment" : "selbst erstellt, OK?", "flags" : "" })
    lemma_obj_list.append({ "text" : "cerna", "ID" : 503677, "color" : "red", "target" : {}, "comment" : "", "flags" : "" })
    lemma_obj_list.append({ "text" : "cernelha", "ID" : 503687, "color" : "green", "target" : {}, "comment" : "", "flags" : "" })
    lemma_obj_list.append({ "text" : "cenofali", "ID" : 502652, "color" : "blue", 
        "target" : { "text" : "cenophali", "ID" : 123456, "color" : "yellow", "target" : {}, "comment" : "", "flags"  : "" }, 
        "comment" : "", "flags" : "" })
    sample_json["ROOT"] = lemma_obj_list
    output_json = json.dumps(sample_json)
    if output == "text":
        print(" [ + ]\tcreated JSON sample text and passed it on")
        return output_json
    else:
        with open("sample.json", "w") as f:
            f.write(output_json)
            f.close()
        print(" [ + ]\tcreated JSON sample file in current working directory")

if __name__ == "__main__":
    # for debugging: comment out undesired value for output
    # output = "text"
    output = "file"
    if output == "text":
        print(" [DBG]\tsample JSON text:")
        print(create_sample_json(output))
    else:
        create_sample_json("text")


# format of JSON to be parsed in real-world-example later:
#         try to just parse the JSON and hope for it to be all wrapped into one mother list
#          => contents = json.loads(...)
#          => mother_key = list(contents.keys())[0]
#          => lemma_obj_list = contents[mother_key]
# [!!!]:  if there are multiple JSON lemma objects present side by side in JSON file, json.loads will raise JSONDecodeError; then do:
#         one JSON mother object list called ROOT : [{<obj1>}, {<obj2>}, {<obj3>} ...]
#           => add '{\n"ROOT" : [\n' to beginning of JSON file and ' \n]\n}' to its end
#           => contents = json.loads(...)
#           => lemma_obj_list = contents["ROOT"]