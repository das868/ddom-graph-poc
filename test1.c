#include <stdio.h>

typedef enum {
    YELLOW,
    GREEN,
    RED,
} Node_Color;

typedef enum {
    BLUE,
    LILA,
    TURKIS,
} Arrow_Color;

typedef long UID;

void inc_uid(UID* uid)
{
    *uid = *uid + 2;
}

typedef struct {
    char* text;
    Node_Color color;
    UID uid;
} Node;

typedef struct {
    Node* root;
    Node* target;
    Arrow_Color color;
    UID uid;
} Arrow;

#define GRAPH_CAP 1024
typedef struct {
    Node* nodes[GRAPH_CAP];
    Arrow* arrows[GRAPH_CAP];
} Graph;

void create_node(UID* nodes_uid, char* text, Node_Color color, Node* dest)
{
    dest->text = text;
    dest->color = color;
    dest->uid = *nodes_uid;
    inc_uid(nodes_uid);
}

void create_arrow(UID* arrows_uid, Node* root, Node* target, Arrow_Color color, Arrow* dest)
{
    dest->root = root;
    dest->target = target;
    dest->color = color;
    dest->uid = *arrows_uid;
    inc_uid(arrows_uid);
}

void create_arrow_from_parts(UID* nodes_uid, UID* arrows_uid, char* root_text, Node_Color root_color, char* target_text, Node_Color target_color, Arrow_Color color, Arrow* dest)
{
    Node root = { .text = root_text, .color = root_color, .uid = *nodes_uid };
    inc_uid(nodes_uid);
    Node target = { .text = target_text, .color = target_color, .uid = *nodes_uid };
    inc_uid(nodes_uid);
    dest->root = &root;
    dest->target = &target;
    dest->color = color;
    dest->uid = *arrows_uid;
    inc_uid(arrows_uid);
}

void print_node(Node* node)
{
    char* col;
    switch(node->color)
    {
        case 0:
            col = "YELLOW";
            break;
        case 1:
            col = "GREEN";
            break;
        case 2:
            col = "RED";
            break;
        default:
            col = "UNDEFINED";
            break;
    }
    printf("[NODE]\ttext: %s -- color: %s -- UID: %ld\n", node->text, col, node->uid);
}

void print_arrow(Arrow* arrow)
{
    char* col;
    switch(arrow->color)
    {
        case 0:
            col = "BLUE";
            break;
        case 1:
            col = "LILA";
            break;
        case 2:
            col = "TURKIS";
            break;
        default:
            col = "UNDEFINED";
            break;
    }
    printf("\n[ARROW]\tcolor: %s -- UID: %ld -- root/target:\n", col, arrow->uid);
    Node root = *(arrow->root);
    Node target = *(arrow->target);
    print_node(&root);
    print_node(&target);
}

void print_graph(Graph* g)
{
    printf("\n------ G R A P H : -----------\n\n");
    
    //iterate through all nodes and print them
    int i = 0;
    while(i < GRAPH_CAP)
    {
        // printf("\tAdress: %x ", g->nodes[i]);
        if(g->nodes[i] == NULL) { break; }
        print_node(g->nodes[i]);
        i++;
    }

    printf("\nJETZT:\n");       // Bug happens in target node of last arrow, reason unknown
    Arrow* temp = g->arrows[1];
    Node* ntemp = temp->target;
    print_node(ntemp);
    return;

    i = 0;
    while(i < GRAPH_CAP) // && g->arrows[i] != 0)
    {
        if(g->arrows[i] == NULL) { break; }
        //Arrow arr = *(g->arrows[i]); 
        // TODO: fix bug introduced here; printing doesn't work, out of bounds
        print_arrow(g->arrows[i]);
        i++;
    }
    printf("\n------------------------------\n\n\n");
}

int main(void)
{
    printf("\n==================PROGRAM===START======================\n");
    UID nodes_uid = 1;  // always odd  non-zero; num of nod`s = (uid + 1) / 2
    UID arrows_uid = 2; // always even non-zero; num of arr`s =     uid   / 2

    Node a;
    create_node(&nodes_uid, "Hello World!", YELLOW, &a);
    print_node(&a);

    Node b;
    create_node(&nodes_uid, "Hello you 2!", GREEN, &b);
    print_node(&b);

    Arrow c;
    create_arrow(&arrows_uid, &a, &b, BLUE, &c);
    print_arrow(&c);

    Arrow d;
    create_arrow_from_parts(&nodes_uid, &arrows_uid, "another 1", GREEN, "another 2", RED, TURKIS, &d);
    print_arrow(&d);

    Graph g;
    g.nodes[(a.uid-1)/2] = &a;
    g.nodes[(b.uid-1)/2] = &b;
    g.arrows[c.uid/2 - 1] = &c;
    g.arrows[d.uid/2 - 1] = &d;

    print_graph(&g);
    return 0;
}